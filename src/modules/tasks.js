// External modules
const cron = require('node-cron');
// Internal modules
const logger = require('./logger');

/**
 * @brief Tableau contenant l'ensemble des tâches créées
 */
module.exports.tasks = [];

/**
 * @brief Crée une tâche qui s'exécutera toutes les minutes. Utilisez la fonction start pout démarrer la tâche
 * @param {function} f Fonction à exécuter toutes les minutes
 * @returns Une tâche node-cron non démarée. Retourne null en cas d'erreur
 */
module.exports.new_minute_task = f => {
    return this.new_custom_task('* * * * *', f);
}

/**
 * @brief Crée une tâche qui s'exécutera toutes les heures. Utilisez la fonction start pout démarrer la tâche
 * @param {function} f Fonction à exécuter toutes les heures
 * @returns Une tâche node-cron non démarée. Retourne null en cas d'erreur
 */
 module.exports.new_hour_task = f => {
    return this.new_custom_task('* 1 * * *', f);
}

/**
 * @brief Crée une tâche qui s'exécutera dans l'intervale de temps indiquée. Utilisez la fonction start pout démarrer la tâche
 * @param {string} time_pattern Chaîne de caractère représentant l'intervalle de temps entre deux exécutions de la tâche. Se référer à la doc de node-cron
 * @param {function} f Fonction à exécuter toutes les heures
 * @returns Une tâche node-cron non démarée. Retourne null en cas d'erreur
 */
 module.exports.new_custom_task = (time_pattern, f) => {
    if (!f) {
        logger.error('Pas de fonction fournie pour node-cron', 'Tasks');
        return null;
    }

    if (!cron.validate(time_pattern)) {
        logger.error('Mauvais pattern du temps pour node-cron', 'Tasks');
        return null;
    }

    const new_task = cron.schedule(time_pattern, f, { scheduled: false });
    this.tasks.push(new_task);

    return new_task;
}

/**
 * @brief Stoppe et supprime la tâche fournie en paramètre
 * @param {object} task Tâche à supprimer
 */
module.exports.remove_task = task => {
    if (!task)
        return ;

    task.stop();

    for(let i = 0; i < this.tasks.length; i++) {
        if (this.tasks[i] === task) {
            this.tasks.splice(i, 1);
            i--;
        }
    }
}

/**
 * @brief Stoppe et détruit toutes les tâches
 */
module.exports.cleanup = _ => {
    this.tasks.forEach(task => {
        if (task != null)
            task.stop();
    });
    this.tasks = [];
}